package com.example.ahmed.demo;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class PauseActivity extends AppCompatActivity {
    Runnable runnable;
    Button resume,home;
    TextView textView;
    int n=4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pause);
        resume= (Button) findViewById(R.id.button6);
        home= (Button) findViewById(R.id.button8);
        textView= (TextView) findViewById(R.id.textView17);
        resume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resume_play();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                home_screen();
            }
        });
    }

    private void home_screen() {
        MainActivity.get_activity().finish();
        finish();
    }

    public void resume_play() {
        final Handler handler=new Handler();
        runnable=new Runnable() {
            @Override
            public void run() {
                n--;
                if(n==0)
                {
                    textView.setText("1");
                    handler.removeCallbacks(runnable);
                    finish();
                }
                else {
                    textView.setText(n + "");
                    handler.postDelayed(runnable, 1000);
                }
            }
        };
        handler.post(runnable);
    }
}
