package com.example.ahmed.demo;


import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;

import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    Handler handler,progress_handler;
    Runnable runnable,runnable2;
    int random_buzzle;
    int random_anime;
    int wrong_button;
    int random_color;
    int progress;
    Random random,rand;
    int Score=0,levelTime=100;
    ProgressBar bar;
    TextView level_number,speed,best_score;
    String Pref_name="Pref_file";
    static MainActivity mainActivity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainActivity=this;
        level_number= (TextView) findViewById(R.id.textView4);
        speed= (TextView) findViewById(R.id.textView5);
        speed.setText("0");
        best_score= (TextView) findViewById(R.id.textView2);
        SharedPreferences prefs = getSharedPreferences(Pref_name, MODE_PRIVATE);
        best_score.setText(prefs.getInt("best_score", 0)+"");
        bar= (ProgressBar) findViewById(R.id.progressBar);
        bar.getProgressDrawable().setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);
        startGameLoop();
        progressBar();
    }

    private void progressBar() {
        progress_handler = new Handler();
        runnable2 = new Runnable()
        {
            @Override
            public void run() {
                level_number.setText(Score+"");
                progress=bar.getProgress();
                if(progress<=0)
                {
                    progress_handler.removeCallbacks(runnable2);
                    handler.removeCallbacks(runnable);
                    Toast.makeText(MainActivity.this,"wrong button : "+wrong_button,Toast.LENGTH_LONG).show();
                    SharedPreferences prefs = getSharedPreferences(Pref_name, MODE_PRIVATE);
                    int current_best=prefs.getInt("best_score",0);
                    if(Score>current_best) {
                        SharedPreferences.Editor editor = getSharedPreferences(Pref_name, MODE_PRIVATE).edit();
                        editor.putInt("best_score", Score);
                        editor.apply();
                    }
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(MainActivity.this, GameOverActivity.class);
                            intent.putExtra("score",Score);
                            finish();
                            startActivity(intent);

                        }
                    });

                }
                else {
                    bar.setProgress(progress - 1);
                    progress_handler.postDelayed(runnable2, levelTime);
                    //Log.d("tag",levelTime+"");
                }
            }
        };
        // Start the Runnable immediately
        progress_handler.post(runnable2);

    }

    @Override
    protected void onPause() {
        progress_handler.removeCallbacks(runnable2);
        super.onPause();

    }

    @Override
    protected void onResume() {
        progress_handler.post(runnable2);
        super.onResume();
    }
    @Override
    public void onBackPressed() {
        progress_handler.removeCallbacks(runnable2);
        Intent intent = new Intent(MainActivity.this, PauseActivity.class);
        startActivity(intent);
    }

    public static MainActivity get_activity()
    {
        return mainActivity;
    }

    private void startGameLoop() {
        handler = new Handler();
        runnable = new Runnable()
        {
            @Override
            public void run() {
                animate();
            }
        };
        // Start the Runnable immediately
        handler.post(runnable);
    }

    public void translateX(View view)
    {
        TranslateAnimation animation=new TranslateAnimation(0,10,0,0);
        animation.setDuration(500);
        animation.setRepeatCount(Animation.INFINITE);
        view.startAnimation(animation);
    }
    public void translateY(View view)
    {
        TranslateAnimation animation=new TranslateAnimation(0,0,0,10);
        animation.setDuration(500);
        animation.setRepeatCount(Animation.INFINITE);
        view.startAnimation(animation);
    }
    public void rotateR(View view)
    {
        RotateAnimation animation = new RotateAnimation(0, 360,Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);
        animation.setDuration(500);
        animation.setRepeatCount(Animation.INFINITE);
        view.startAnimation(animation);
    }
    public void rotateL(View view)
    {
        RotateAnimation animation = new RotateAnimation(360, 0,Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);
        animation.setDuration(500);
        animation.setRepeatCount(Animation.INFINITE);
        view.startAnimation(animation);
    }
    public void translateX2(View view)// for wrong button
    {
        TranslateAnimation animation=new TranslateAnimation(0,10,0,0);
        animation.setDuration(480);
        animation.setRepeatCount(Animation.INFINITE);
        view.startAnimation(animation);
    }
    public void translateY2(View view)// for wrong button
    {
        TranslateAnimation animation=new TranslateAnimation(0,0,0,10);
        animation.setDuration(480);
        animation.setRepeatCount(Animation.INFINITE);
        view.startAnimation(animation);
    }
    public void rotateR2(View view)// for wrong button
    {
        RotateAnimation animation = new RotateAnimation(0, 360,Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);
        animation.setDuration(450);
        animation.setRepeatCount(Animation.INFINITE);
        view.startAnimation(animation);
    }
    public void rotateL2(View view)// for wrong button
    {
        RotateAnimation animation = new RotateAnimation(360, 0,Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);
        animation.setDuration(450);
        animation.setRepeatCount(Animation.INFINITE);
        view.startAnimation(animation);
    }
    public void changeColor(ImageButton view,int color)
    {
        String background="arrow_up";
        int resID = getResources().getIdentifier(background+color, "drawable", getPackageName());
        view.setImageResource(resID);
    }
    public void animate() {

        random = new Random();
        random_buzzle = random.nextInt(2) + 1;
        random_anime = random.nextInt(4) + 1;
        wrong_button = random.nextInt(35)+1;
        random_color=random.nextInt(6)+1;
        String buttonID = "";
        for (int x = 1; x <= 35; x++) {
            buttonID = "imageButton" + x;
            int resID = getResources().getIdentifier(buttonID, "id", getPackageName());
            ImageButton button = (ImageButton) findViewById(resID);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    checkAnswer(view);
                }
            });
            changeColor(button,random_color);
            if(random_buzzle==1) {
                if (random_anime == 1)
                    rotateR(button);
                else if (random_anime == 2)
                    rotateL(button);
                else if (random_anime == 3)
                    translateX(button);
                else if (random_anime == 4)
                    translateY(button);

                if (x == wrong_button)
                    button.setAlpha(0.70f);
            }
            else {
                if (x != wrong_button) {
                    if (random_anime == 1)
                        rotateR(button);
                    else if (random_anime == 2)
                        rotateL(button);
                    else if (random_anime == 3)
                        translateX(button);
                    else if (random_anime == 4)
                        translateY(button);
                } else {
                    if (random_anime == 1)
                        rotateL2(button);
                    else if (random_anime == 2)
                        rotateR2(button);
                    else if (random_anime == 3)
                        translateX2(button);
                    else if (random_anime == 4)
                        translateY2(button);
                }
            }
        }
    }
    private void checkAnswer(View view) {
        if (bar.getProgress()<=0)
            return;
        String buttonID = "imageButton" + wrong_button;
        if (view.getId() == getResources().getIdentifier(buttonID, "id", getPackageName())) {
            bar.setProgress(100);
            Score++;
            int speed = Score / 10;
            if (speed >5)
                speed = 5;
            levelTime = 100 - (15 * speed);
            if(speed==5)
                this.speed.setTextColor(Color.RED);
            this.speed.setText(speed + "");
                    cancelAnimations();
            handler.post(runnable);
        }
    }
    private void cancelAnimations() {
        String buttonID = "";
        for (int x = 1; x <= 35; x++) {
            buttonID = "imageButton" + x;
            int resID = getResources().getIdentifier(buttonID, "id", getPackageName());
            ImageButton button = (ImageButton) findViewById(resID);
            button.clearAnimation();
            button.setAlpha(1f);
        }
    }
}
