package com.example.ahmed.demo;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class StartGameActivity extends AppCompatActivity {
    Button startButton;
    ImageButton instructions,exitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_game);
        animate_buttons();
        startButton=(Button)findViewById(R.id.button);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(StartGameActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
        instructions=(ImageButton) findViewById(R.id.button4);
        instructions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*
                Dialog dialog = new Dialog(StartGameActivity.this); // Context, this, etc.
                dialog.setContentView(R.layout.activity_tutorial);
                dialog.setTitle("Instructions");
                dialog.show();*/
                final DialogFragment dialog=new Instructions_dialog();
                dialog.show(getSupportFragmentManager(),"tag");

            }
        });
        exitButton=(ImageButton) findViewById(R.id.button5);
        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                //finish();
            }
        });
    }

    @Override
    public void onBackPressed() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(StartGameActivity.this);
        builder.setTitle("Exit?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
        //finish();
    }

    private void animate_buttons() {
        MainActivity mainActivity=new MainActivity();
        String buttonID;
        for (int x = 36; x <= 40; x++) {
            buttonID = "imageButton" + x;
            int resID = getResources().getIdentifier(buttonID, "id", getPackageName());
            ImageButton button = (ImageButton) findViewById(resID);
            mainActivity.rotateR(button);
        }
        for (int x = 41; x <= 45; x++) {
            buttonID = "imageButton" + x;
            int resID = getResources().getIdentifier(buttonID, "id", getPackageName());
            ImageButton button = (ImageButton) findViewById(resID);
            mainActivity.translateX(button);
        }

        for (int x = 46; x <= 50; x++) {
            buttonID = "imageButton" + x;
            int resID = getResources().getIdentifier(buttonID, "id", getPackageName());
            ImageButton button = (ImageButton) findViewById(resID);
            mainActivity.rotateL(button);
        }

        for (int x = 51; x <= 55; x++) {
            buttonID = "imageButton" + x;
            int resID = getResources().getIdentifier(buttonID, "id", getPackageName());
            ImageButton button = (ImageButton) findViewById(resID);
            mainActivity.translateY(button);
        }
    }
}
